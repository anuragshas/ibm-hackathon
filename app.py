import os
from flask import Flask,render_template, request,json,jsonify
import urllib3, requests, json, datetime,math

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/crop_predict', methods=['POST'])
def crop_predict():
    x = request.get_json()
    V2 = x['Nitrogen']
    V3 = x['Phosphorous']
    V4 = x['potassium']
    V5 = x['organiccarbon']
    V6 = x['pH']
    V7 = x['temp']
 
    wml_credentials={"url": "https://ibm-watson-ml.mybluemix.net","username": "2290bd6b-328f-47a5-a56d-d2c8cb53c78d","password": "2182a54c-6fa2-4248-a62f-7740b89b145c"}
    headers = urllib3.util.make_headers(basic_auth='{username}:{password}'.format(username=wml_credentials['username'], password=wml_credentials['password']))
    url = '{}/v3/identity/token'.format(wml_credentials['url'])
    response = requests.get(url, headers=headers)
    mltoken = json.loads(response.text).get('token')
    header = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + mltoken}
    payload_scoring = {"fields": ["Crop", "Nitrogen", "Phosphorous", "potassium", "organiccarbon", "pH", "temp"], "values": [(1,V2,V3,V4,V5,V6,V7),(2,V2,V3,V4,V5,V6,V7),(3,V2,V3,V4,V5,V6,V7)]}
    response_scoring = requests.post('https://ibm-watson-ml.mybluemix.net/v3/wml_instances/fc91ce84-3224-45c7-987a-9818d438e1df/published_models/bfc81978-70b5-446e-b2df-3c2fcd6c3246/deployments/020a0836-b2de-4910-97fc-bc07260eb27b/online', json=payload_scoring, headers=header)
    recieved = json.loads(response_scoring.text)
    data = {'data':[
        {'id':"Wheat",
            'yield':recieved['values'][0][-1]
        },{'id':"Paddy",
            'yield':recieved['values'][1][-1]
        },{'id':"Maize",
            'yield':recieved['values'][2][-1]
        }]}
    return json.dumps(data)
    
@app.route('/risk_assess', methods=['POST'])
def risk_assess():
    x = request.get_json()
    state = int(x['State'])
    
    wml_credentials={"url": "https://ibm-watson-ml.mybluemix.net","username": "2290bd6b-328f-47a5-a56d-d2c8cb53c78d","password": "2182a54c-6fa2-4248-a62f-7740b89b145c"}
    headers = urllib3.util.make_headers(basic_auth='{username}:{password}'.format(username=wml_credentials['username'], password=wml_credentials['password']))
    url = '{}/v3/identity/token'.format(wml_credentials['url'])
    response = requests.get(url, headers=headers)
    mltoken = json.loads(response.text).get('token')
    header = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + mltoken}
    date = datetime.datetime.now()
    payload_scoring = {"fields": ["State", "Year", "Month"], "values": [(state, date.year,date.month),(state, date.year,date.month+1),(state, date.year,date.month+2),(state, date.year,date.month+3),(state, date.year,date.month+4),(state, date.year,date.month+5)]}
    response_scoring = requests.post('https://ibm-watson-ml.mybluemix.net/v3/wml_instances/fc91ce84-3224-45c7-987a-9818d438e1df/published_models/a7093ff2-09c1-4ff7-a8c9-21966e69cf75/deployments/284781e4-2940-4473-b8aa-4609ebc2366c/online', json=payload_scoring, headers=header)
    recieved = json.loads(response_scoring.text)
    print(recieved)
    average = (recieved['values'][0][-1]+recieved['values'][1][-1]+recieved['values'][2][-1]+recieved['values'][3][-1]+recieved['values'][4][-1]+recieved['values'][5][-1])/6
    range_val = [[60,90],[120,140],[60,110]]
    rainfall = average
    crop = int(x['crop']) - 1
    risk = 0
    if rainfall>=range_val[crop][1]:
	    risk = (rainfall - range_val[crop][1])/5.0
	    if risk>100:
	        risk=100.0
    elif rainfall<=range_val[crop][0]:
        risk = (range_val[crop][0]-rainfall)/2.5
        if risk>100:
            risk=100.0
    else:
        risk = 0

    data = {'rainfall':average,'risk':risk}
    return json.dumps(data)

def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295     #Pi/180
    a = 0.5 - math.cos((lat2 - lat1) * p)/2 + math.cos(lat1 * p) * math.cos(lat2 * p) * (1 - math.cos((lon2 - lon1) * p)) / 2
    return 12742 * math.asin(math.sqrt(a))
	
@app.route('/storage', methods=['POST'])
def storage():
    x = request.get_json()
    lat1 = float(x['lat'])
    log1 = float(x['lng'])
    amount = int(x['weight'])
    days = int(x['time'])
    fp = open('latlon.csv','r')
    city_details = fp.readline()
    cities = []
    while city_details: 
        cities.append(city_details)
        city_details = fp.readline()
    city_dist = []
    for j in range(len(cities)):
        row2 = cities[j].split(",")
        cost = int(row2[3][:len(row2[3])-1])
        lat2 = float(row2[1])
        log2 = float(row2[2])
        dist = distance(lat1, log1, lat2, log2)
        city_dist.append((dist,cities[j],cost))
    city_dist.sort()
    nearest10_city  = city_dist[:10]
    Cost_to_store = []
    for i in range(len(nearest10_city)):
        store_cost = nearest10_city[i][2]*days*amount
        Cost_to_store.append((store_cost,nearest10_city[i][1]))
    
    Cost_to_store.sort()
    top5_city_to_store = Cost_to_store[:5]
    top5_city = []
    for i in range(5):
        city_info = top5_city_to_store[i][1]
        city_info = city_info[:len(city_info)-1].split(",")
        city_info.append(top5_city_to_store[i][0])
        top5_city.append(city_info)
    data = {'data':[
			{'loc1':top5_city[0]},
			{'loc2':top5_city[1]},   
			{'loc3':top5_city[2]},
			{'loc4':top5_city[3]},
			{'loc5':top5_city[4]}
			]
		   }
    return json.dumps(data)
if __name__=="__main__":
    app.run()
