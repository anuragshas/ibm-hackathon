angular.module('myapp')
    .controller('riskAssessmentController', ['$scope','$http','$filter',riskAssessmentController]);

function riskAssessmentController($scope, $http,$filter){
    this.scope = $scope;
    this.scope.crops = [
        {
            id:1,
            name: "Wheat"
        },
        {
            id:2,
            name: "Paddy"
        },
        {
            id:3,
            name: "Maize"
        }
    ];
    this.scope.states = ['SUB HIMALAYAN WEST BENGAL & SIKKIM', 'GANGETIC WEST BENGAL', 'VIDARBHA', 'ARUNACHAL PRADESH', 'BIHAR', 'KONKAN & GOA', 'KERALA', 'NORTH INTERIOR KARNATAKA', 'JHARKHAND', 'PUNJAB', 'EAST MADHYA PRADESH', 'CHHATTISGARH', 'MATATHWADA', 'LAKSHADWEEP', 'RAYALSEEMA', 'WEST RAJASTHAN', 'WEST MADHYA PRADESH', 'EAST UTTAR PRADESH', 'TAMIL NADU', 'TELANGANA', 'SAURASHTRA & KUTCH', 'COASTAL ANDHRA PRADESH', 'SOUTH INTERIOR KARNATAKA', 'ANDAMAN & NICOBAR ISLANDS', 'GUJARAT REGION', 'NAGA MANI MIZO TRIPURA', 'MADHYA MAHARASHTRA', 'ASSAM & MEGHALAYA', 'UTTARAKHAND', 'COASTAL KARNATAKA', 'EAST RAJASTHAN', 'HARYANA DELHI & CHANDIGARH', 'HIMACHAL PRADESH', 'JAMMU & KASHMIR', 'WEST UTTAR PRADESH', 'ORISSA'];
    this.scope.predictCrop = function(){
        $http({
            url: "/risk_assess",
            method: "POST",
            data:{
                "State": $scope.state,
                "crop": $scope.crop
            }
        }).then(function mySuccess(response) {
            $scope.risk_obtained = response.data;
            console.log($scope.risk_obtained);
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
        });
    }
}
