angular.module('myapp')
    .controller('storageController', ['$scope','$http','$filter',storageController]);

function storageController($scope, $http,$filter){
    this.scope = $scope;
    this.scope.predictStorage = function(){
        $http({
            url: "/storage",
            method: "POST",
            data:{
                "lat": $scope.lat,
                "lng": $scope.lng,
                "weight": $scope.weight,
                "time": $scope.time
            }
        }).then(function mySuccess(response) {
            $scope.storage_ob = response.data;
            console.log($scope.storage_ob);
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
        });
    }
}
