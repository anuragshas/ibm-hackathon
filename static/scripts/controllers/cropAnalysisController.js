angular.module('myapp')
    .controller('cropAnalysisController', ['$scope','$http','$filter',cropAnalysisController]);

function cropAnalysisController($scope, $http,$filter){
    this.scope = $scope;
    this.scope.crops = [
        {
            id:1,
            name: "Wheat"
        },
        {
            id:2,
            name: "Paddy"
        },
        {
            id:3,
            name: "Maize"
        }
    ];
    $scope.yeildsSorted = null;
    this.scope.predictCrop = function(){
        $http({
            url: "/crop_predict",
            method: "POST",
            data:{
                "Nitrogen": $scope.nitrogen,
                "Phosphorous": $scope.phosphorus,
                "potassium":$scope.potassium,
                "organiccarbon":$scope.organiccarbon,
                "pH":$scope.pH,
                "temp":$scope.temp}
        }).then(function mySuccess(response) {
            $scope.yields = response.data.data;
            sortedArray = $scope.yields.sort(function(a,b){
                return a.yield<b.yield?1:a.yield >b.yield?-1:0
            });
            console.log($scope.yields);
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
        });
    }
}
