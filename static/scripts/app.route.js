angular.module('myapp')
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider,$locationProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                views: {
                    'home': {
                        templateUrl: 'static/views/home.html',
                        controller: 'homeController'
                    }
                }
            })
            .state('soil',{
                url: '/soil',
                views: {
                    'content@': {
                        templateUrl: 'static/views/soil.html',
                        controller: 'homeController'
                    }
                }
            })
            .state('products',{
                url:'/products',
                views:{
                    'products':{
                        templateUrl: 'static/views/products.html',
                        controller: 'homeController'
                    }
                }
            })
            .state('predictors',{
                url: '/predictors',
                views: {
                    'content@': {
                        templateUrl: 'static/views/predictor.html',
                        controller: 'homeController'
                    }
                }
            })
            .state('crop_analysis',{
                url: '/crop_analysis',
                views: {
                    'content@': {
                        templateUrl: 'static/views/crop_analysis.html',
                        controller: 'cropAnalysisController'
                    }
                }
            })
            .state('risk_assessment',{
                url: '/risk_assessment',
                views: {
                    'content@': {
                        templateUrl: 'static/views/risk_assessment.html',
                        controller: 'riskAssessmentController'
                    }
                }
            })
            .state('storage_assessment',{
                url: '/storage_assessment',
                views: {
                    'content@': {
                        templateUrl: 'static/views/storage.html',
                        controller: 'storageController'
                    }
                }
            })
        ;
        $urlRouterProvider.otherwise('/');
    });
